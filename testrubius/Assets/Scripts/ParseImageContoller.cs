using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum LoadType { Step, Parallel, All }
public class ParseImageContoller : MonoBehaviour
{
    [Header("Data setting")]
    [SerializeField] private  Image[]  icons;
    [SerializeField] private string url;
    private ILoadImage LoadImageType;
    [Header("Support element")]
    static public int counter;
    [Header("Events")]
    public static UnityEvent onImageLoad = new UnityEvent();
    public static UnityEvent<LoadType> onChangeType = new UnityEvent<LoadType>();
    

    private void Awake()
    {
        onImageLoad.AddListener(SetLoadImage);
        onChangeType.AddListener(SetLogic);
    }

    public void SetLoadImage()
    {
        if (counter <= icons.Length - 1)
        {
            StartCoroutine(LoadImageType.LoadImage(url, icons[counter], counter));
        }
        else
        {
            counter = 0;
        }
    }

    private void SetLogic(LoadType _type)
    {
        switch (_type)
        {
            case LoadType.Step:
                LoadImageType = new StepLoad();
                break;
            case LoadType.All:
                LoadImageType = new ParallelLoad();
                break;
            case LoadType.Parallel:
                LoadImageType = new CompleteLoad();
                break;
            default:
                break;
        }
    }

    public void LoadImage()
    {
        Debug.Log(CardAnimationController.isCardTurn);

        if (CardAnimationController.isCardTurn)
        {
            for (int i = 0; i < icons.Length; i++)
                StartCoroutine(CardAnimationController.StartPositoinCard(i));
            
        }
        else
        {
            SetLoadImage();
        }
    }

    public void CancelLoad()
    {
        StopAllCoroutines();
        counter = 0;
    }
}