using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class CardAnimationController : MonoBehaviour
{
    [Header("Static data")]
    static public List<Card> cards = new List<Card>();
    static public bool isCardTurn;
    [Header("Data setting")]
    [SerializeField] List<Card> cardList;
    [SerializeField] float delay;
    [Header("Supportnt element")]
    static int counter;
    [Header("Event")]
    public static UnityEvent<int> onCardFlip = new UnityEvent<int>();

    void Start()
    {
        foreach (var card in cardList)
            cards.Add(card);
        

        onCardFlip.AddListener(FlipCard);
        ManagerUI.onLoadComplete.AddListener(CardReady);
    }

    public void SetCard(GameObject back, GameObject front)
    {
        back.SetActive(false);
        front.SetActive(true);
    }

    void FlipCard(int index) => StartCoroutine(FlipCardCoritine(index));

    IEnumerator FlipCardCoritine(int index)
    {
        counter++;
        var card = cards[index];
        var agree = Quaternion.Euler(0, 90, 0);
        Tween flip = card.bodyCard.transform.DOLocalRotateQuaternion(agree, delay);
        yield return flip.WaitForCompletion();
        isCardTurn = false;
        SetCard(card.backCard, card.frontCard);
        flip = card.bodyCard.transform.DOLocalRotateQuaternion(Quaternion.Euler(0, 180, 0), delay);
        yield return flip.WaitForCompletion();
        isCardTurn = true;

        if (counter >= cards.Count)
        {
            ManagerUI.onLoadComplete.Invoke();
            counter = 0;
        }
    }

    static public IEnumerator StartPositoinCard(int index)
    {
        var card = cards[index];
        var agree = Quaternion.Euler(0, -90, 0);
        Tween flip = card.bodyCard.transform.DOLocalRotateQuaternion(agree, 0.5f);
        yield return flip.WaitForCompletion();
        card.backCard.SetActive(true);
        card.frontCard.SetActive(false);
        flip = card.bodyCard.transform.DOLocalRotateQuaternion(Quaternion.Euler(0, 0, 0), 0.5f);
        yield return flip.WaitForCompletion();
        counter++;
        isCardTurn = false;

        if (counter == cards.Count)
        {
            counter = 0;
            ParseImageContoller.onImageLoad.Invoke();
        }
    }

    public void CancelLoad()
    {
        StopAllCoroutines();

        foreach (var card in cards)
            StartCoroutine(BackCardCoritine(card));
        
    }

    IEnumerator BackCardCoritine(Card card)
    {
        if (card.bodyCard.transform.localRotation.y > 90)
        {
            var agree = Quaternion.Euler(0, 90, 0);
            Tween flip = card.bodyCard.transform.DOLocalRotateQuaternion(agree, delay);
            yield return flip.WaitForCompletion();
            card.backCard.SetActive(true);
            card.frontCard.SetActive(false);
            agree = Quaternion.Euler(0, 0, 0);
            card.bodyCard.transform.DOLocalRotateQuaternion(agree, delay);
        }
        else
        {
            card.backCard.SetActive(true);
            card.frontCard.SetActive(false);
            var agree = Quaternion.Euler(0, 0, 0);
            card.bodyCard.transform.DORotateQuaternion(agree, delay);
            isCardTurn = false;
        }
    }

    void OnDestroy() => cards.Clear();

    void CardReady() => isCardTurn = true;
}

[Serializable]
public class Card
{
    public GameObject frontCard;
    public GameObject backCard;
    public GameObject bodyCard;

    public Card()
    {
        frontCard = null;
        backCard = null;
        bodyCard = null;
    }

    public Card(GameObject _frontCard, GameObject _backCard, GameObject _bodyCard)
    {
        frontCard = _frontCard;
        backCard = _backCard;
        bodyCard = _bodyCard;
    }
}