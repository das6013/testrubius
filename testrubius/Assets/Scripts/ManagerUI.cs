using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour
{
    [Header("Data element")]
    private LoadType loadType;
    [Header("UI element")]
    [SerializeField] Dropdown dropdown;
    [SerializeField] Button CancelButton;
    [SerializeField] Button StartButton;
    [Header("Event")]
    static public UnityEvent onLoadComplete = new UnityEvent();

    void Start()
    {
        onLoadComplete.AddListener(TurntButton);
        DropDownSelected(dropdown);

        dropdown.onValueChanged.AddListener(delegate
        {
            DropDownSelected(dropdown);
        });
    }

    void DropDownSelected(Dropdown dropdown)
    {
        var index = dropdown.value;
        SetTypeLoad(index);
    }

    public void SetTypeLoad(int _type)
    {
        switch (_type)
        {
            case 0:
                loadType = LoadType.All; break;
            case 1:
                loadType = LoadType.Step; break;
            case 2:
                loadType = LoadType.Parallel; break;
            default:
                break;
        }

        ParseImageContoller.onChangeType.Invoke(loadType);
    }

    public void TurntButton()
    {
        StartButton.interactable = !StartButton.interactable;
        CancelButton.interactable = !CancelButton.interactable;
    }
}