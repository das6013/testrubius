using System.Collections;
using UnityEngine.UI;

public interface ILoadImage
{
    public IEnumerator LoadImage(string url, Image icon, int index);
}
