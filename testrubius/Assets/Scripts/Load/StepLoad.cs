using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StepLoad : ILoadImage
{
    public IEnumerator LoadImage(string url, Image icon, int index)
    {
        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
        }
        else
        {
            var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2());
            icon.sprite = sprite;
            ParseImageContoller.counter++;
            ParseImageContoller.onImageLoad.Invoke();
            CardAnimationController.onCardFlip.Invoke(index);
        }
    }
}