using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ParallelLoad : ILoadImage
{
    static int counter;
    public IEnumerator LoadImage(string url, Image icon, int index)
    {
        ParseImageContoller.counter++;
        ParseImageContoller.onImageLoad.Invoke();
        var www = UnityWebRequestTexture.GetTexture(url);
        www.timeout = 2;
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
        }
        else
        {
            var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            var sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2());
            icon.sprite = sprite;
            counter++;

            if (counter == CardAnimationController.cards.Count - 1)
            {
                for (int i = 0; i < counter + 1; i++)
                    CardAnimationController.onCardFlip.Invoke(i);
                

                counter = 0;
            }
        }
    }
}